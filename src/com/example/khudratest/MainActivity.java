package com.example.khudratest;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie2;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.itemdto.ItemDTO;

public class MainActivity extends Activity {
	private Map<String, ItemDTO> items = new HashMap<String, ItemDTO>();
	private TableLayout item_table;
	private String URL = "http://basket-asapweb.rhcloud.com/baskets/?username=basket&password=changeme&date=03/28/2014&pricetype=W";
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// initialize the singleton
		sInstance = this;
		setContentView(R.layout.table_view);
		item_table = (TableLayout) findViewById(R.id.item_table);
		pd = ProgressDialog.show(this, "Loading...", "Items are Loading...");
		// final JSONParser parse = new JSONParser();
		JsonObjectRequest req = new JsonObjectRequest(URL, null,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						try {
							VolleyLog.v("Response:%n %s", response.toString(4));
							parseJSONObject(response);
							fillItemTable();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e("Error: ", error.getMessage());
					}
				});

		// add the cookie before adding the request to the queue
		// instance.setCookie();

		// add the request object to the queue to be executed
		sInstance.addToRequestQueue(req);

	}

	private void parseJSONObject(JSONObject json) throws JSONException {
		// JSONObject value = json.getJSONObject("value");
		JSONArray json_array = json.getJSONArray("baskets");
		for (int i = 0; i < json_array.length(); i++) {
			ItemDTO item = new ItemDTO();
			JSONObject readerObject = (JSONObject) json_array.get(i);
			String name = readerObject.getString("name").toUpperCase();
			String unit = readerObject.getString("unit");
			long cur_maxm = readerObject.getLong("maximum");
			long cur_minm = readerObject.getLong("minimum");
			long cur_price = readerObject.getLong("average");
			// TODO prevprice work on
			long prevprice = 0;
			long prev_maxm = 0;
			long prev_minm = 0;

			long diff = cur_price - prevprice;

			item.setItemName(name);

			item.setUnit(unit);

			item.setItem_Cur_Price(Long.toString(cur_price));
			item.setItem_Prev_Price(Long.toString(prevprice));
			item.setDiff(Long.toString(diff));

			item.setItem_Min_Cur_Price(Long.toString(cur_minm));
			item.setItem_Max_Cur_Price(Long.toString(cur_maxm));

			item.setItem_Min_Prev_Price(Long.toString(prev_minm));
			item.setItem_Max_Prev_Price(Long.toString(prev_maxm));

			this.items.put(name, item);

		}

	}

	void fillItemTable() {

		TableRow row;
		TextView item_logo, item_name, cur_price, unit, up_down;
		// Converting to dip unit
		int dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				(float) 1, getResources().getDisplayMetrics());

		for (Entry<String, ItemDTO> itemlist : items.entrySet()) {
			row = new TableRow(this);
			String key_item = itemlist.getKey();
			ItemDTO itemdto = itemlist.getValue();

			// TODO set image for item_logo and up_down
			item_logo = new TextView(this);
			item_name = new TextView(this);
			cur_price = new TextView(this);
			unit = new TextView(this);
			up_down = new TextView(this);

			item_logo.setText(itemdto.getItemName());
			item_name.setText(itemdto.getItemName());
			cur_price.setText(itemdto.getItem_Cur_Price());
			unit.setText(itemdto.getUnit());
			up_down.setText(itemdto.getDiff());

			item_logo.setTextColor(getResources().getColor(R.color.Yellow));
			item_name.setTextColor(getResources().getColor(R.color.DarkRed));
			cur_price.setTextColor(getResources().getColor(R.color.Yellow));
			unit.setTextColor(getResources().getColor(R.color.DarkRed));
			// TODO set color acc to diff
			up_down.setTextColor(getResources().getColor(
					(itemdto.getDiff().contains("-")) ? R.color.Red
							: R.color.Green));

			item_logo.setTypeface(null, 1);
			item_name.setTypeface(null, 1);
			cur_price.setTypeface(null, 1);
			unit.setTypeface(null, 1);
			up_down.setTypeface(null, 1);

			item_logo.setTextSize(15);
			item_name.setTextSize(15);
			cur_price.setTextSize(15);
			unit.setTextSize(15);
			up_down.setTextSize(15);

			item_logo.setWidth(50 * dip);
			item_name.setWidth(130 * dip);
			cur_price.setWidth(50 * dip);
			unit.setWidth(50 * dip);
			up_down.setWidth(50 * dip);

			up_down.setPadding(20 * dip, 0, 0, 0);

			row.addView(item_logo);
			row.addView(item_name);
			row.addView(cur_price);
			row.addView(unit);
			row.addView(up_down);

			item_table.addView(row, new TableLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Log or request TAG
	 */
	public static final String TAG = "VolleyPatterns";

	/**
	 * Global request queue for Volley
	 */
	private RequestQueue mRequestQueue;

	/**
	 * A singleton instance of the application class for easy access in other
	 * places
	 */
	private static MainActivity sInstance;

	/**
	 * @return MainActivity singleton instance
	 */
	public static synchronized MainActivity getInstance() {
		return sInstance;
	}

	// http client instance
	private DefaultHttpClient mHttpClient;

	public RequestQueue getRequestQueue() {
		// lazy initialize the request queue, the queue instance will be
		// created when it is accessed for the first time
		if (mRequestQueue == null) {
			// Create an instance of the Http client.
			// We need this in order to access the cookie store
			mHttpClient = new DefaultHttpClient();
			// create the request queue
			mRequestQueue = Volley.newRequestQueue(MainActivity.this,
					new HttpClientStack(mHttpClient));
			// mRequestQueue = Volley.newRequestQueue(MainActivity.this);
		}
		return mRequestQueue;
	}

	/**
	 * Method to set a cookie
	 */
	public void setCookie() {
		CookieStore cs = mHttpClient.getCookieStore();
		// create a cookie
		cs.addCookie(new BasicClientCookie2("cookie", "spooky"));
	}

	/**
	 * Adds the specified request to the global queue, if tag is specified then
	 * it is used else Default TAG is used.
	 * 
	 * @param req
	 * @param tag
	 */
	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

		VolleyLog.d("Adding request to queue: %s", req.getUrl());

		getRequestQueue().add(req);
	}

	/**
	 * Adds the specified request to the global queue using the Default TAG.
	 * 
	 * @param req
	 * @param tag
	 */
	public <T> void addToRequestQueue(Request<T> req) {
		// set the default tag if tag is empty
		req.setTag(TAG);

		getRequestQueue().add(req);
	}

	/**
	 * Cancels all pending requests by the specified TAG, it is important to
	 * specify a TAG so that the pending/ongoing requests can be cancelled.
	 * 
	 * @param tag
	 */
	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

}
