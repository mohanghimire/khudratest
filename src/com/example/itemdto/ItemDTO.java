package com.example.itemdto;

import java.io.Serializable;

public class ItemDTO implements Serializable {
	//TODO itemLogo
	private String itemLogo = new String();
	private String itemName = new String();
	
	private String item_Prev_Price = new String();
	private String item_Min_Prev_Price = new String();
	private String item_Max_Prev_Price = new String();
	
	private String item_Cur_Price = new String();
	private String item_Min_Cur_Price = new String();
	private String item_Max_Cur_Price = new String();
	
	private String diff = new String();
	
	private String unit =  new String();

	public ItemDTO() {
		super();
	}

	public String getItemLogo() {
		return itemLogo;
	}

	public void setItemLogo(String itemLogo) {
		this.itemLogo = itemLogo;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItem_Prev_Price() {
		return item_Prev_Price;
	}

	public void setItem_Prev_Price(String item_Prev_Price) {
		this.item_Prev_Price = item_Prev_Price;
	}

	public String getItem_Min_Prev_Price() {
		return item_Min_Prev_Price;
	}

	public void setItem_Min_Prev_Price(String item_Min_Prev_Price) {
		this.item_Min_Prev_Price = item_Min_Prev_Price;
	}

	public String getItem_Max_Prev_Price() {
		return item_Max_Prev_Price;
	}

	public void setItem_Max_Prev_Price(String item_Max_Prev_Price) {
		this.item_Max_Prev_Price = item_Max_Prev_Price;
	}

	public String getItem_Cur_Price() {
		return item_Cur_Price;
	}

	public void setItem_Cur_Price(String item_Cur_Price) {
		this.item_Cur_Price = item_Cur_Price;
	}

	public String getItem_Min_Cur_Price() {
		return item_Min_Cur_Price;
	}

	public void setItem_Min_Cur_Price(String item_Min_Cur_Price) {
		this.item_Min_Cur_Price = item_Min_Cur_Price;
	}

	public String getItem_Max_Cur_Price() {
		return item_Max_Cur_Price;
	}

	public void setItem_Max_Cur_Price(String item_Max_Cur_Price) {
		this.item_Max_Cur_Price = item_Max_Cur_Price;
	}

	public String getDiff() {
		return diff;
	}

	public void setDiff(String diff) {
		this.diff = diff;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}


}
