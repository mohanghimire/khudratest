package com.example.volley;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.itemdto.ItemDTO;
import com.example.khudratest.MainActivity;
import com.example.khudratest.StartActivity;

public class JSONParser extends Application {
	private static Map<String, ItemDTO> items = new HashMap<String, ItemDTO>();
	private String URL = "http://basket-asapweb.rhcloud.com/baskets/?username=basket&password=changeme&date=03/28/2014&pricetype=W";
	private RequestQueue mRequestQueue;

	public void parseJSON() {
		if (!items.isEmpty()) {
			items.clear();
		}
		JsonObjectRequest req = new JsonObjectRequest(URL, null,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						try {
							VolleyLog.v("Response:%n %s", response.toString(4));
							parseJSONObject(response);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e("Error: ", error.getMessage());
					}
				});

		StartActivity instance = StartActivity.getInstance();
		// add the cookie before adding the request to the queue
		// instance.setCookie();

		// add the request object to the queue to be executed
		instance.addToRequestQueue(req);
	}

	private void parseJSONObject(JSONObject json) throws JSONException {
		// JSONObject value = json.getJSONObject("value");
		JSONArray json_array = json.getJSONArray("baskets");
		for (int i = 0; i < json_array.length(); i++) {
			ItemDTO item = new ItemDTO();
			JSONObject readerObject = (JSONObject) json_array.get(i);
			String name = readerObject.getString("name").toUpperCase();
			String unit = readerObject.getString("unit");
			long cur_maxm = readerObject.getLong("maximum");
			long cur_minm = readerObject.getLong("minimum");
			long cur_price = readerObject.getLong("average");
			// TODO prevprice work on
			long prevprice = 0;
			long prev_maxm = 0;
			long prev_minm = 0;

			long diff = cur_price - prevprice;

			item.setItemName(name);

			item.setUnit(unit);

			item.setItem_Cur_Price(Long.toString(cur_price));
			item.setItem_Prev_Price(Long.toString(prevprice));
			item.setDiff(Long.toString(diff));

			item.setItem_Min_Cur_Price(Long.toString(cur_minm));
			item.setItem_Max_Cur_Price(Long.toString(cur_maxm));

			item.setItem_Min_Prev_Price(Long.toString(prev_minm));
			item.setItem_Max_Prev_Price(Long.toString(prev_maxm));

			items.put(name, item);
			// this.setItems(items);
			System.out.println("Item Name" + name);

		}

	}

	public Map<String, ItemDTO> getItems() {
		return items;
	}

	public void setItems(Map<String, ItemDTO> items) {
		this.items = items;
	}

	public String getURL() {
		return this.URL;
	}

	public void setURL(String uRL) {
		this.URL = uRL;
	}

}