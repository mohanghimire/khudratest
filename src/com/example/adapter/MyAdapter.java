package com.example.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.itemdto.ItemDTO;

public class MyAdapter extends BaseAdapter {
	private final List<ItemDTO> vegdto = new ArrayList<ItemDTO>();
	private Context eContext;

	public MyAdapter(Context eContext) {
		this.eContext = eContext;
	}

	public void add(ItemDTO item) {
		vegdto.add(item);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return vegdto.size();
	}

	@Override
	public Object getItem(int pos) {
		return vegdto.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		View view = convertView;
		if(view==null){
			
		}
		
		// TODO Auto-generated method stub
		return null;
	}

}
